package API;

public interface IQueue <T>{

	public void  enqueue(T item);
	public boolean isEmpty();
	public T dequeue();
	public int size();
	
}
