package logic;

import java.util.ArrayList;

import model.data_structures.Stack;

public class ChocoChevere {
	
	public boolean expresionBienFormada(Stack input){
		boolean tiene=false;
		Stack ugly=input;
		for(int i=0;i<=ugly.size();i++){
			if(ugly.pop().equals('[')){
				for(int j=0;j<=ugly.size();j++){
					if(ugly.pop().equals(']')){
						tiene = true;
						break;
					}
				}
				if(!tiene){
					return false;
				}
			}
			ugly=input;
			for(int x=0;x<=ugly.size();x++){
				if(ugly.pop().equals('(')){
					for(int j=0;j<=ugly.size();j++){
						if(ugly.pop().equals(')')){
							tiene = true;
							break;
						}
					}
					if(!tiene){
						return false;
					}
				}
			}
			
		}
		return true;
	}
	
	public Stack ordenarPila(Stack input){
		Stack nueva = null, vieja = input;
		ArrayList sym = null, ope=null, numeros=null;
		
			for(int i=0;i<=vieja.size();i++){
				Object foo=vieja.pop();
				
				if(foo.equals('[')||foo.equals(']')||foo.equals('{')||foo.equals('}')
						||foo.equals('(')||foo.equals(')')){
					sym.add(foo);
				}
				
				if(foo.equals('0')||foo.equals('1')||foo.equals('2')||foo.equals('3')
						||foo.equals('4')||foo.equals('5')||foo.equals('6')||foo.equals('7')||foo.equals('8')||foo.equals('9')){
					numeros.add(foo);
				}
				
				if(foo.equals('+')||foo.equals('-')||foo.equals('*')||foo.equals('/')){
					ope.add(foo);
				}
			}
			for(int i=0;i<numeros.size();i++){
				nueva.push(numeros.get(i));
			}
			for(int i=0;i<ope.size();i++){
				nueva.push(ope.get(i));
			}
			for(int i=0;i<sym.size();i++){
				nueva.push(sym.get(i));
			}
			return nueva;
	}
	
}