package model.data_structures;

import API.IStack;

public class Stack <T> implements IStack{

	private Node<T> first;
	private Node<T> last;
	private Node<T> actual;
	public int size;
	public int posicion;

	public Stack(){
		first = null;
		last = null;
		actual = null;
		size=0;
		posicion=0;
	}

	@SuppressWarnings("unchecked")
	public void push(Object item) {
		first = (Node<T>) item;
		size++;
	}

	public Object pop() {
		Node<T> returner= first;
		first= first.darAnterior();
		size--;
		return returner;

	}

	public boolean isEmpty() {
		if(first!=null||last!=null){
			return false;
		}else{
			return true;
		}
	}

	public int size() {
		return size;
	}





}
