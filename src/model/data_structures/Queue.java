package model.data_structures;

import API.IQueue;

public class Queue <T> implements IQueue {


	private Node<T> first;
	private Node<T> last;
	private Node<T> actual;
	public int size;
	public int posicion;

	public Queue(){
		first=null;
		last=null;
		actual=null;	
	}
	@Override
	public void enqueue(Object item) {
		first = (Node<T>) item;
		size++;

	}

	@Override
	public boolean isEmpty() {
		if(first!=null||last!=null){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public Object dequeue() {
		Node<T> returner= last;
		last= last.darAnterior();
		size--;
		return returner;

	}

	@Override
	public int size() {

		return size;
	}

}
