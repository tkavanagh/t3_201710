package model.data_structures;


public class Node<T> {
	

		private T element;
		private Node<T> previous;
		private Node<T> next;

	
		public Node ( T pElement){
			element = pElement;
			next = null;
			previous = null;
		}

		public Node<T> darAnterior(){
			return previous;
		}
		
		public Node<T> dar(){
			return this;
		}
		

		public Node<T> darSiguiente(){
			return next;
		}
		
		public void cambiarSiguiente(T cosa){
			next.element = cosa;
		}
		
		public void cambiarAnterior(T cosa){
			previous.element = cosa;
		}
	}

